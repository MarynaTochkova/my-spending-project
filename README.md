# README #

"My Spending" is interactive noncommercial website where one can record an account spending monthly, prioritize, group data, see and edit historical data.  

This project will help: 
- recording an accounting spending in a convenient table monthly
- prioritizing and grouping data
- adding new custom groups of spending
- editing data if it's needed
- controlling the total cost after each purchase
- storing data in the Archive

Link to the site: http://test-021.azurewebsites.net