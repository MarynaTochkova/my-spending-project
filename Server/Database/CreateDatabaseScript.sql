use MySpending
GO

CREATE TABLE [dbo].[User]
(
Id INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
Login varchar(64) NOT NULL UNIQUE,
Password varchar(255) NOT NULL,
Email nvarchar(64) NOT NULL UNIQUE 
);
GO


CREATE TABLE [dbo].[Current]
(
Id INT NOT NULL PRIMARY KEY IDENTITY(1,1) ,
UserId INT NOT NULL REFERENCES [dbo].[User](Id),
Year INT NULL,
Month INT NULL
)
GO

CREATE TABLE [dbo].[SpendingType]
(
Id int PRIMARY KEY IDENTITY(1,1) NOT NULL,
Name nvarchar(128) NOT NULL,
UserId INT FOREIGN KEY REFERENCES [dbo].[User](Id)
)
GO


CREATE TABLE [dbo].[Spending]
(
Id int PRIMARY KEY IDENTITY(1,1) NOT NULL,
Date datetime2(4) NOT NULL,
Name nvarchar(256) NOT NULL,
Cost money NOT NULL,
IsImportant bit NOT NULL,
SpendingTypeId INT FOREIGN KEY REFERENCES [dbo].[SpendingType](Id) NOT NULL,
UserId INT FOREIGN KEY REFERENCES [dbo].[User](Id) NOT NULL
)
GO