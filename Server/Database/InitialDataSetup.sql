﻿use MySpending
GO
SET IDENTITY_INSERT [dbo].[User] ON

INSERT INTO [dbo].[User](Id,[Login],[Password],Email) VALUES (0,'system-user','','system.user@email.com')

SET IDENTITY_INSERT [dbo].[User] OFF
GO


SET IDENTITY_INSERT [dbo].[SpendingType] ON

INSERT INTO [dbo].[SpendingType] (Id,Name, UserId)
VALUES
(1,'Food (excl. restaurants)', 0),
(2,'Restaurants costsС', 0),
(3,'Clothing and shoes', 0),
(4,'Transport (public)', 0),
(5,'Own car, bike, etc. (excl. insurance)', 0),
(6,'Entertainment (cinema, theater, etc.)', 0),
(7,'Small household expenses (cleaning products,etc.)', 0),
(8,'Big household expenses (furniture, repairs, household appliances, etc.)', 0),
(9,'Personal care (cosmetic products, beauty products, salon)', 0),
(10,'Hobby (gym membership, etc.)', 0),
(11,'Medicines', 0),
(12,'Health (medical consultation)', 0),
(13,'Rent (apartment, car, etc.)', 0),
(14,'Other services (bank services, laundry, tailoring, post, etc.)', 0),
(15,'Insurance (all kinds)', 0),
(16,'Presents', 0),
(17,'Gadgets and internet (their purchase and payment)', 0),
(18,'Utility payments (gas, electricity, etc.)', 0),
(19,'Education (fee for courses or University, etc.)', 0),
(20,'Taxes', 0),
(21,'Travelling', 0)

SET IDENTITY_INSERT [dbo].[SpendingType] OFF
GO
