USE MySpending
GO

DELETE  FROM [Current]
GO


DELETE FROM [Spending]
DBCC CHECKIDENT ([Spending], reseed, 1)
GO

DELETE FROM [SpendingType]
DBCC CHECKIDENT ([SpendingType], reseed, 1)
GO

DELETE FROM [User]
DBCC CHECKIDENT ([User], reseed, 1)
GO
