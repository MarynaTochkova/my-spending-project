﻿using External.Implementation.DataAccess;

namespace Internal.Contract
{
    public interface ICurrentService
    {
        Current Get(int userId);

        void Update(Current current);
    }
}
