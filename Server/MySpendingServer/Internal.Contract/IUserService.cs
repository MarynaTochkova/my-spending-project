﻿
using External.Implementation.DataAccess;

namespace Internal.Contract
{
    public interface IUserService
    {
        bool VerifyUser(User user, string password);
        void CreateUser(User user);
        User Get(string login);
        int GetUserId(string login);
        bool UserExist(string login, string email);
    }
}
