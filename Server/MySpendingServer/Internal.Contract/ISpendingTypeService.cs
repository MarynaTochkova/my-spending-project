﻿using System.Collections.Generic;
using External.Implementation.DataAccess;

namespace Internal.Contract
{
    public interface ISpendingTypeService
    {
        IEnumerable<SpendingType> Get(int userId);

        IEnumerable<SpendingType> GetDefault();

        int Insert(SpendingType item);
    }
}
