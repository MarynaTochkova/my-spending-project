﻿
namespace Internal.Contract
{
    public interface IDefaultSettingsProvider
    {
        int SystemUserId { get; }

        int TestUserId { get; }
    }
}
