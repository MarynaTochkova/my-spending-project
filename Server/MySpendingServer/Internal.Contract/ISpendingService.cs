﻿using System.Collections.Generic;
using External.Implementation.DataAccess;

namespace Internal.Contract
{
    public interface ISpendingService
    {
        IEnumerable<Spending> Get(int userId, int month, int year);
        void Delete(int userId, int spendingId);

        int Add(Spending spending);
        void Update(Spending spending);
        IEnumerable<RangeModel> GetRange(int userId);
    }
}
