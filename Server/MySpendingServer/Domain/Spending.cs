﻿using System;

namespace Domain
{
    public class Spending
    {
        public int? Id { get; set; }

        public DateTime Date { get; set; }

        public int SpengingTypeId { get; set; }

        public string Name { get; set; }

        public decimal Cost { get; set; }

        public bool IsImportant { get; set; }

    }
}
