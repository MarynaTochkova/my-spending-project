﻿using System;

namespace Domain
{
    public class SpendingType
    {
        public int? Id { get; set; }

        public string Name { get; set; }
    }
}
