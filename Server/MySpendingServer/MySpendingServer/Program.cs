﻿using System;
using Microsoft.Owin.Hosting;

namespace MySpendingServer
{
    class Program
    {
        static void Main(string[] args)
        {
            string baseAddress = "http://localhost:9000/";
            using (WebApp.Start<Startup>(url: baseAddress))
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("Types can be found here http://localhost:9000/api/types");
                Console.WriteLine("Server running at {0} - press Enter to quit. ", baseAddress);
                Console.ReadLine(); 

            }
        }
    }
}
