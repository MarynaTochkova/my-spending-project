﻿using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using MySpendingServer.Bootstrapper;
using MySpendingServer.DI.Unity;
using Owin;

namespace MySpendingServer
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
           
            appBuilder.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            // Configure Web API for self-host. 
            var config = new HttpConfiguration();
            //config.DependencyResolver = new UnityDependencyResolver(UnityHelpers.GetConfiguredContainer());
            var iocConfig = new UnityConfiguration();
            config.DependencyResolver = new UnityDependencyResolver(iocConfig.BuildContainer());

            config.Routes.MapHttpRoute(
            name: "DefaultApiWithActions",
            routeTemplate: "api/{controller}/{action}/{id}",
            defaults: new { id = RouteParameter.Optional });

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


            var mapping = new ObjectMapperConfiguration();
            mapping.Configure();
          

            appBuilder.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/api/auth/login"),
                CookieSecure = CookieSecureOption.SameAsRequest,
                //CookieDomain = "http://localhost:63342/"
            });
            appBuilder.UseWebApi(config);
        }
    }
}
