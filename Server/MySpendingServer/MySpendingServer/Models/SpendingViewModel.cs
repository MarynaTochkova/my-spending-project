﻿using System;

namespace MySpendingServer.Models
{
   public class SpendingViewModel
   {
       public int? Id { get; set; }

       public DateTime Date { get; set; }

       public int? SpendingTypeId { get; set; }

       public string SpendingTypeName { get; set; }

       public string Name { get; set; }

       public decimal Cost { get; set; }

       public bool IsImportant { get; set; }
   }
}
