﻿using System;
using System.Web.Helpers;
using External.Implementation.DataAccess;
using Internal.Contract;

namespace MySpendingServer.Infrastructure
{
    public class UserService: IUserService
    {
        private readonly IUserRepository _repository;
        private readonly IDefaultSettingsProvider _defaultSettingsProvider;
        public UserService(IUserRepository repository, IDefaultSettingsProvider defaultSettingsProvider)
        {
            _repository = repository;
            _defaultSettingsProvider = defaultSettingsProvider;
        }
        public bool VerifyUser(User user, string password)
        {
            if (user.Id == _defaultSettingsProvider.SystemUserId)
            {
                throw new Exception("Access denied");
            }
            return Crypto.VerifyHashedPassword(user.Password, password);
        }

        public void CreateUser(User user)
        {
            user.Password = Crypto.HashPassword(user.Password);
            _repository.Insert(user);
        }

        public User Get(string login)
        {
            return _repository.GetByLogin(login);
        }

        public bool UserExist(string login, string email)
        {
            return _repository.UserExists(login, email);
        }


        public int GetUserId(string login)
        {
            return Get(login).Id;
        }
    }
}
