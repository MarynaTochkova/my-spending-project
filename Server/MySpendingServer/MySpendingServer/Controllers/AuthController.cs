﻿using System;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using Internal.Contract;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using MySpendingServer.Models;

namespace MySpendingServer.Controllers
{
    [AllowAnonymous]
    public class AuthController: ApiController
    {
        private readonly IUserService _userService;

        public AuthController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public void Login()
        {
            throw new HttpResponseException(HttpStatusCode.Forbidden);
        }

        [HttpPost]
        public UserViewModel Login(LoginModel model)
        {
            var user = _userService.Get(model.Login);

            if (_userService.VerifyUser(user, model.Password))
            {
                var identity = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, model.Login),
                    new Claim(ClaimTypes.NameIdentifier, model.Login), 
                }, DefaultAuthenticationTypes.ApplicationCookie);
                Request.GetOwinContext()
                    .Authentication.SignIn(new AuthenticationProperties {IsPersistent = true, ExpiresUtc = DateTime.UtcNow.AddMinutes(30)}, identity);

                return new UserViewModel {Login = user.Login, UserId = user.Id, Email = user.Email};
            }
            throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Forbidden,
                "Invalid user name or password. Please try again."));
        }

        [HttpGet]
        public void Logout()
        {
           Request.GetOwinContext()
                    .Authentication.SignOut();
        }
    }
}
