﻿using System.Collections.Generic;
using System.Web.Http;
using External.Implementation.DataAccess;
using External.Mapper;
using Internal.Contract;
using MySpendingServer.Models;

namespace MySpendingServer.Controllers
{
    [AllowAnonymous]
    public class MonthController : ApiController
    {
        private readonly ISpendingService _spendingService;
        private readonly IObjectMapper _mapper;
        private readonly IDefaultSettingsProvider _defaultSettingsProvider;

        public MonthController(ISpendingService spendigService, IObjectMapper mapper, IDefaultSettingsProvider defaultSettingsProvider)
        {
            _spendingService = spendigService;
            _mapper = mapper;
            _defaultSettingsProvider = defaultSettingsProvider;
        }

        // TODO! - default user Maryna has id of 35
        // default month is current month and year 


        [HttpGet]
        public IEnumerable<MonthViewModel> Get()
        {
            var range = _spendingService.GetRange(_defaultSettingsProvider.TestUserId);
            return _mapper.Map<IEnumerable<RangeModel>, IEnumerable<MonthViewModel>>(range);
        }
    }
}
