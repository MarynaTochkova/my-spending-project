﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using External.Implementation.DataAccess;
using External.Mapper;
using Internal.Contract;
using MySpendingServer.Models;

namespace MySpendingServer.Controllers
{
    //[Authorize]
    public class TypesController : ApiController
    {
        private ISpendingTypeService _typeService;
        private IObjectMapper _mapper;
        private readonly IDefaultSettingsProvider _defaultSettingsProvider;

        //TODO: Insert USer selection
        //TODO: Auth

        public TypesController(ISpendingTypeService typeService, IObjectMapper mapper, IDefaultSettingsProvider defaultSettingsProvider)
        {
            _typeService = typeService;
            _mapper = mapper;
            _defaultSettingsProvider = defaultSettingsProvider;
        }

        [HttpGet]
        public IEnumerable<SpendingTypeViewModel> Get()
        {
            var userTypes = _typeService.Get(_defaultSettingsProvider.TestUserId);
            var types = userTypes.Union(_typeService.GetDefault()).OrderBy(x => x.Id);
            return _mapper.Map<IEnumerable<SpendingType>, List<SpendingTypeViewModel>>(types);
        }

        [HttpPost]
        public int Post([FromBody]SpendingTypeViewModel type)
        {
            //get user from auth
            var item = new SpendingType();
            item.UserId = _defaultSettingsProvider.TestUserId;
            item = _mapper.Map(type, item);
            return _typeService.Insert(item);
        }
    }
}
