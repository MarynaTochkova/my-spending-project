﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using External.Implementation.DataAccess;
using Internal.Contract;
using MySpendingServer.Models;
using IObjectMapper = External.Mapper.IObjectMapper;

namespace MySpendingServer.Controllers
{
   //[Authorize]
    public class SpendingController: ApiController
    {
        private readonly ISpendingService _spendingService;
        private IObjectMapper _mapper;
        private ISpendingTypeService _spTypeService;
        private readonly IDefaultSettingsProvider _defaultSettingsProvider;

        //TODO: Insert USer selection
        //TODO: Auth

        public SpendingController(ISpendingService spendingService, IObjectMapper mapper, 
            ISpendingTypeService spendingTypeService, IDefaultSettingsProvider defaultSettingsProvider)
        {
            _spendingService = spendingService;
            _mapper = mapper;
            _spTypeService = spendingTypeService;
            _defaultSettingsProvider = defaultSettingsProvider;
        }
 
        public IEnumerable<SpendingViewModel> Get([FromUri]MonthViewModel model)
        {
            var spendings = _spendingService.Get(_defaultSettingsProvider.TestUserId, model.Month, model.Year);

            var types = _spTypeService.Get(_defaultSettingsProvider.TestUserId);
            types = types.Union(_spTypeService.GetDefault());

            return spendings.Join(types, sp => sp.SpendingTypeId, tp => tp.Id,
             (sp, tp) => new SpendingViewModel
             {
                 Id = sp.Id,
                 Cost = sp.Cost,
                 Name = sp.Name,
                 Date = sp.Date,
                 IsImportant = sp.IsImportant,
                 SpendingTypeId = sp.SpendingTypeId,
                 SpendingTypeName = tp.Name
             }).OrderBy(x => x.Date);
        }

        [HttpPost]
        public int Post([FromBody]SpendingViewModel spending)
        {
            var item = _mapper.Map<SpendingViewModel, Spending>(spending);
            item.UserId = _defaultSettingsProvider.TestUserId;
            return _spendingService.Add(item);
        }

        [HttpPut]
        public void Put([FromBody]SpendingViewModel spending)
        {

            if (spending.Id.HasValue)
            {
                var item = _mapper.Map<SpendingViewModel, Spending>(spending);
                item.UserId = _defaultSettingsProvider.TestUserId;
                _spendingService.Update(item);
            }
        }

        [HttpDelete]
        public void Delete(int id)
        {
            _spendingService.Delete(_defaultSettingsProvider.TestUserId, id);
        }
    }
}
