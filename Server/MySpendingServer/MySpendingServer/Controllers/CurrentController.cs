﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using External.Implementation.DataAccess;
using External.Mapper;
using Internal.Contract;
using MySpendingServer.Models;

namespace MySpendingServer.Controllers
{
    public class CurrentController: ApiController
    {

        private readonly IDefaultSettingsProvider _defaultSettingsProvider;
        private readonly ICurrentService _currentService;
        private readonly IObjectMapper _mapper;

        public CurrentController(IDefaultSettingsProvider defaultSettingsProvider, ICurrentService currentService, IObjectMapper mapper)
        {
            _defaultSettingsProvider = defaultSettingsProvider;
            _currentService = currentService;
            _mapper = mapper;
        }

        [HttpPost]
        public void Set(MonthViewModel model)
        {
            var current = _mapper.Map<MonthViewModel, Current>(model);
            current.UserId = _defaultSettingsProvider.TestUserId;
            _currentService.Update(current);
        }

        [HttpGet]
        public MonthViewModel Get()
        {
            return _mapper.Map<Current, MonthViewModel>(_currentService.Get(_defaultSettingsProvider.TestUserId));
        }
    }
}
