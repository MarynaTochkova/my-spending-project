﻿using Internal.Contract;

namespace Internal.Implementation
{
    public class DefaultSettingsProvider: IDefaultSettingsProvider
    {
        private const int SystemUserIdId = 0;

        private const int TestUserIdId = 1;

        public int SystemUserId
        {
            get { return SystemUserIdId; }
        }

        public int TestUserId
        {
            get { return TestUserIdId; }
        }
    }
}
