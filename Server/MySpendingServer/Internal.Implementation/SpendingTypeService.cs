﻿using System.Collections.Generic;
using External.Implementation.DataAccess;
using Internal.Contract;

namespace Internal.Implementation
{
    public class SpendingTypeService: ISpendingTypeService
    {
        private readonly ISpendingTypeRepository _repository;
        private readonly IDefaultSettingsProvider _defaultSettingsProvider;

        public SpendingTypeService(ISpendingTypeRepository repository, IDefaultSettingsProvider defaultSettingsProvider)
        {
            _repository = repository;
            _defaultSettingsProvider = defaultSettingsProvider;
        }

        public IEnumerable<SpendingType> Get(int userId)
        {
            return _repository.Get(userId);
        }

        public IEnumerable<SpendingType> GetDefault()
        {
            return _repository.Get(_defaultSettingsProvider.SystemUserId);
        }

        public int Insert(SpendingType item)
        {
            return _repository.Insert(item);
        }
    }
}
