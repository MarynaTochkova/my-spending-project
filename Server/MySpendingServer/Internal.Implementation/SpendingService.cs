﻿using System;
using System.Collections.Generic;
using External.Implementation.DataAccess;
using Internal.Contract;

namespace Internal.Implementation
{
    public class SpendingService : ISpendingService
    {
        private ISpendingRepository _repository;
        //use repository + POSTGRESE SQL + MicroORM;

        public SpendingService(ISpendingRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Spending> Get(int userId, int month, int year)
        {
            return  _repository.Get(userId, month, year);
        }

        public void Delete(int userId, int spendingId)
        {
            _repository.Delete(userId, spendingId);
        }

        public int Add(Spending spending)
        {
           return _repository.Insert(spending);
        }

        public void Update(Spending spending)
        {
            _repository.Update(spending);
        }

        public IEnumerable<RangeModel> GetRange(int userId)
        {
            return _repository.GetRange(userId);
        }
    }
}
