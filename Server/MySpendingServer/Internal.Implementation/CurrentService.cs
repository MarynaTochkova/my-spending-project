﻿using External.Implementation.DataAccess;
using Internal.Contract;

namespace Internal.Implementation
{
    public class CurrentService: ICurrentService
    {
        private readonly ICurrentRepository _repository;
        public CurrentService(ICurrentRepository repository)
        {
            _repository = repository;
        }
        public Current Get(int userId)
        {
            return _repository.Get(userId);
        }

        public void Update(Current current)
        {
            _repository.Update(current);
        }
    }
}
