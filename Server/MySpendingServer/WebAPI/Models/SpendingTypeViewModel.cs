﻿namespace WebAPI.Models
{
    public class SpendingTypeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
