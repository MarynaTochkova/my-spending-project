﻿using System.Runtime.Serialization;

namespace WebAPI.Models
{
    [DataContract]
    public class CreateUserViewModel
    {
        [DataMember(Name = "login")]
        public string Login { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }
    }
}
