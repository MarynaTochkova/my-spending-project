﻿using System.Runtime.Serialization;

namespace WebAPI.Models
{
    [DataContract]
    public class MonthViewModel
    {
        [DataMember(Name = "month", IsRequired = true)]
        public int Month { get; set; }

        [DataMember(Name = "year", IsRequired = true)]
        public int Year { get; set; }
    }
}
