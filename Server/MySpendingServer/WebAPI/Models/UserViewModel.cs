﻿using System.Runtime.Serialization;

namespace WebAPI.Models
{
    [DataContract]
    public class UserViewModel
    {
        [DataMember(Name = "userid")]
        public int UserId { get; set; }

        [DataMember(Name = "login")]
        public string Login { get; set; }

        [DataMember(Name="email")]
        public string Email { get; set; }
    }
}
