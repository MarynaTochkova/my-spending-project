﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using External.Implementation.DataAccess;
using External.Mapper;
using Internal.Contract;
using WebAPI.Models;

namespace WebAPI.Controllers
{
   [Authorize]
    public class SpendingController: ApiController
    {
        private readonly ISpendingService _spendingService;
        private IObjectMapper _mapper;
        private ISpendingTypeService _spTypeService;
        private readonly IUserService _userService;


        public SpendingController(ISpendingService spendingService, IObjectMapper mapper,
            ISpendingTypeService spendingTypeService, IUserService userService)
        {
            _spendingService = spendingService;
            _mapper = mapper;
            _spTypeService = spendingTypeService;
            _userService = userService;
        }
 
       [HttpGet]
        public IEnumerable<SpendingViewModel> Get([FromUri]MonthViewModel model)
        {
            var userId = _userService.GetUserId(User.Identity.Name);
            var spendings = _spendingService.Get(userId, model.Month, model.Year);

            var types = _spTypeService.Get(userId);
            types = types.Union(_spTypeService.GetDefault());

            return spendings.Join(types, sp => sp.SpendingTypeId, tp => tp.Id,
             (sp, tp) => new SpendingViewModel
             {
                 Id = sp.Id,
                 Cost = sp.Cost,
                 Name = sp.Name,
                 Date = sp.Date,
                 IsImportant = sp.IsImportant,
                 SpendingTypeId = sp.SpendingTypeId,
                 SpendingTypeName = tp.Name
             }).OrderBy(x => x.Date);
        }

        [HttpPost]
        public int Post([FromBody]SpendingViewModel spending)
        {
            var item = _mapper.Map<SpendingViewModel, Spending>(spending);
            item.UserId = _userService.GetUserId(User.Identity.Name);
            return _spendingService.Add(item);
        }

        [HttpPut]
        public void Put([FromBody]SpendingViewModel spending)
        {
            if (spending.Id.HasValue)
            {
                var item = _mapper.Map<SpendingViewModel, Spending>(spending);
                item.UserId = _userService.GetUserId(User.Identity.Name);
                _spendingService.Update(item);
            }
        }

        [HttpDelete]
        public void Delete(int id)
        {
            _spendingService.Delete(_userService.GetUserId(User.Identity.Name), id);
        }
    }
}
