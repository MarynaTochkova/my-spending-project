﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using External.Implementation.DataAccess;
using External.Mapper;
using Internal.Contract;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    public class TypesController : ApiController
    {
        private ISpendingTypeService _typeService;
        private IObjectMapper _mapper;
        private readonly IUserService _userService;


        public TypesController(ISpendingTypeService typeService, IObjectMapper mapper, IUserService userService)
        {
            _typeService = typeService;
            _mapper = mapper;
            _userService = userService;
        }

        [HttpGet]
        public IEnumerable<SpendingTypeViewModel> Get()
        {
            
            var userTypes = _typeService.Get(_userService.GetUserId(User.Identity.Name));
            var types = userTypes.Union(_typeService.GetDefault()).OrderBy(x => x.Id);
            return _mapper.Map<IEnumerable<SpendingType>, List<SpendingTypeViewModel>>(types);
        }

        [HttpPost]
        public int Post([FromBody]SpendingTypeViewModel type)
        {
           //TODO: User
            var item = new SpendingType();
            item.UserId = _userService.GetUserId(User.Identity.Name);
            item = _mapper.Map(type, item);
            return _typeService.Insert(item);
        }
    }
}
