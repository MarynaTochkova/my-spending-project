﻿using System.Collections.Generic;
using System.Web.Http;
using External.Implementation.DataAccess;
using External.Mapper;
using Internal.Contract;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    public class MonthController : ApiController
    {
        private readonly ISpendingService _spendingService;
        private readonly IObjectMapper _mapper;
        private readonly IUserService _userService;

        public MonthController(ISpendingService spendigService, IObjectMapper mapper, IUserService userService)
        {
            _spendingService = spendigService;
            _mapper = mapper;
            _userService = userService;
        }


        [HttpGet]
        public IEnumerable<MonthViewModel> Get()
        {
            var range = _spendingService.GetRange(_userService.GetUserId(User.Identity.Name));
            return _mapper.Map<IEnumerable<RangeModel>, IEnumerable<MonthViewModel>>(range);
        }
    }
}
