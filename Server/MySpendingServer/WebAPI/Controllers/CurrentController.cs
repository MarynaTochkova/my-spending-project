﻿using System.Web.Http;
using External.Implementation.DataAccess;
using External.Mapper;
using Internal.Contract;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Authorize]
    public class CurrentController: ApiController
    {
        private readonly ICurrentService _currentService;
        private readonly IObjectMapper _mapper;
        private readonly IUserService _userService;

        public CurrentController(ICurrentService currentService, IObjectMapper mapper, IUserService userService)
        {
            _currentService = currentService;
            _mapper = mapper;
            _userService = userService;
        }

        [HttpPost]
        public void Set(MonthViewModel model)
        {
            var current = _mapper.Map<MonthViewModel, Current>(model);
            current.UserId = _userService.GetUserId(User.Identity.Name);
            _currentService.Update(current);
        }

        [HttpGet]
        public MonthViewModel Get()
        {
            return _mapper.Map<Current, MonthViewModel>(_currentService.Get(_userService.GetUserId(User.Identity.Name)));
        }
    }
}
