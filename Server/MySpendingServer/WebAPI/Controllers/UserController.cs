﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using External.Implementation.DataAccess;
using External.Mapper;
using Internal.Contract;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [AllowAnonymous]
    public class UserController: ApiController
    {
        private readonly IUserService _userService;
        private readonly IObjectMapper _mapper;
        public UserController(IUserService userService, IObjectMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [HttpPost]
        public HttpResponseMessage CreateUser(CreateUserViewModel model)
        {
            if (_userService.UserExist(model.Login, model.Email))
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.Conflict,
                "This user is already exist. Please create another user again."));
            }
            var user = _mapper.Map<CreateUserViewModel, User>(model);
            _userService.CreateUser(user);

            return new HttpResponseMessage(HttpStatusCode.Created);
        }

        //public void ForgetPassword()
        //{
            
        //}
    }
}
