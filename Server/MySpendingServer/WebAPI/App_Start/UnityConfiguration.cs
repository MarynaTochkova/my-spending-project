﻿using External.Implementation.DataAccess;
using External.Implementation.Mapper;
using External.Mapper;
using Internal.Contract;
using Internal.Implementation;
using Microsoft.Practices.Unity;
using WebAPI.Infrastructure;

namespace WebAPI
{
    class UnityConfiguration
    {
        public IUnityContainer BuildContainer()
        {
            var container = new UnityContainer();
            container.RegisterType<ISpendingService, SpendingService>();
            container.RegisterType<ISpendingTypeService, SpendingTypeService>();
            //  container.RegisterType<IRepository, EFRepository>(new TransientLifetimeManager());
            container.RegisterType<ISpendingTypeRepository, SpendingTypeRepository>();
            container.RegisterType<IObjectMapper, ObjectMapper>();
            container.RegisterType<ISpendingRepository, SpendingRepository>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IUserRepository, UserRepository>();
            container.RegisterType<IDefaultSettingsProvider, DefaultSettingsProvider>();
            container.RegisterType<ICurrentRepository, CurrentRepository>();
            container.RegisterType<ICurrentService, CurrentService>();

            return container;
        }
    }
}
