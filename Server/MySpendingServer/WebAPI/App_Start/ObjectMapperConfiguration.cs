﻿using System;
using AutoMapper;
using External.Implementation.DataAccess;
using WebAPI.Models;

namespace WebAPI
{
   class ObjectMapperConfiguration
   {
       public void Configure()
       {
           Mapper.CreateMap<SpendingType, SpendingTypeViewModel>();
           Mapper.CreateMap<SpendingTypeViewModel, SpendingType>();

           Mapper.CreateMap<SpendingViewModel, Spending>();
           Mapper.CreateMap<RangeModel, MonthViewModel>();

           Mapper.CreateMap<CreateUserViewModel, User>();

           Mapper.CreateMap<Current, MonthViewModel>();
           Mapper.CreateMap<MonthViewModel, Current>();
               
           Mapper.CreateMap<RangeModel, MonthViewModel>();
       }
   }
}
