﻿using External.Mapper;

namespace External.Implementation.Mapper
{
    public class ObjectMapper : IObjectMapper
    {
      
        public TDest Map<TSrc, TDest>(TSrc source)
        {
            return AutoMapper.Mapper.Map<TSrc, TDest>(source);
        }

        public TDest Map<TSrc, TDest>(TSrc source, TDest destination)
        {
            return AutoMapper.Mapper.Map(source, destination);
        }
    }
}
