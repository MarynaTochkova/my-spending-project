﻿using System.Collections.Generic;

namespace External.Implementation.DataAccess
{
    public interface ISpendingRepository
    {
        IEnumerable<Spending> Get(int userId, int month, int year);

        int Insert(Spending spending);

        void Delete(int userId, int spendingId);

        void Update(Spending spending);

        IEnumerable<RangeModel> GetRange(int userId);
    }
}
