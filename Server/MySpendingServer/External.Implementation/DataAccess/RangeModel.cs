﻿
namespace External.Implementation.DataAccess
{
    public class RangeModel
    {
        public int Month { get; set; }

        public int Year { get; set; }
    }
}
