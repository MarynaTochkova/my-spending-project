﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace External.Implementation.DataAccess
{
    public class SpendingRepository: ISpendingRepository
    {
        public IEnumerable<Spending> Get(int userId, int month, int year)
        {
            using (var context = new MySpendingEntities())
            {
                return context.Spendings.
                    Where(x => x.UserId == userId && x.Date.Month == month && x.Date.Year == year).AsNoTracking().ToList();
            }
        }

        public int Insert(Spending spending)
        {
            using (var context = new MySpendingEntities())
            {
                context.Spendings.Add(spending);
                context.SaveChanges();
                return spending.Id;
            }
        }

        public void Delete(int userId, int spendingId)
        {
            using (var context = new MySpendingEntities())
            {
                var spending = context.Spendings.
                    FirstOrDefault(x => x.Id == spendingId && x.UserId == userId);
                context.Spendings.Remove(spending);
                context.SaveChanges();
            }
        }

        public void Update(Spending spending)
        {
            using (var context = new MySpendingEntities())
            {
                var sp = context.Spendings.Single(x => x.Id == spending.Id);
                sp.Cost = spending.Cost;
                sp.Date = spending.Date;
                sp.Name = spending.Name;
                sp.IsImportant = spending.IsImportant;
                sp.SpendingTypeId = spending.SpendingTypeId;
                context.SaveChanges();
            }
        }

        public IEnumerable<RangeModel> GetRange(int userId)
        {
            using (var context = new MySpendingEntities())
            {
                var selection = from p in context.Spendings.Where(x => x.UserId == userId).AsNoTracking()
                                group p by new { p.Date.Month, p.Date.Year }
                                    into dt
                                    select new RangeModel { Month = dt.FirstOrDefault().Date.Month, Year = dt.FirstOrDefault().Date.Year }
                                    ;

                return selection.ToList();
            }
        }
    }
}
