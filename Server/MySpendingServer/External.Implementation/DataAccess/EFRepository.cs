﻿using System;
using System.Collections.Generic;
using External.DataAccess;

namespace External.Implementation.DataAccess
{
    public class EFRepository: IRepository
    {
        public TEntity Single<TEntity, TId>(TId id) where TEntity : class
        {
            using (var context = new MySpendingEntities())
            {
                var entity = context.Set<TEntity>();
                return null;
            }
            
        }

        public TEntity SingleOrDefault<TEntity, TId>(TId id) where TEntity : class
        {
            throw new NotImplementedException();
        }

        public IReadOnlyCollection<TEntity> Select<TEntity>(IQuery<TEntity> query) where TEntity : class
        {
            throw new NotImplementedException();
        }

        public void Insert<TEntity>(TEntity entity) where TEntity : class
        {
            using (var context = new MySpendingEntities())
            {
                var entities = context.Set<TEntity>();
                entities.Create();
            }
        }

        public void Update<TEntity>(TEntity entity) where TEntity : class
        {
            throw new NotImplementedException();
        }

        public void Delete<TEntity>(TEntity entity) where TEntity : class
        {
            throw new NotImplementedException();
        }
    }
}
