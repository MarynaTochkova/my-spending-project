﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace External.Implementation.DataAccess
{
    public class SpendingTypeRepository : ISpendingTypeRepository
    {
        public IEnumerable<SpendingType> Get(int userId)
        {
            using (var context = new MySpendingEntities())
            {
                return context.SpendingTypes.Where(x => x.UserId == userId).AsNoTracking().ToList();
            }
        }

        public int Insert(SpendingType spType)
        {
            using (var context = new MySpendingEntities())
            {
                context.SpendingTypes.Add(spType);
                context.SaveChanges();
                return spType.Id;
            }
        }
    }
}
