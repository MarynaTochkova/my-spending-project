﻿using System.Linq;

namespace External.Implementation.DataAccess
{
    public class CurrentRepository: ICurrentRepository
    {
        public Current Get(int userId)
        {
            using (var context = new MySpendingEntities())
            {
                return context.Currents.FirstOrDefault(x => x.UserId == userId);
            }
        }

        public void Update(Current current)
        {
            using (var context = new MySpendingEntities())
            {
                var record =  context.Currents.FirstOrDefault(x => x.UserId == current.UserId);
                if (record == null)
                {
                    context.Currents.Add(current);
                    context.SaveChanges();
                }
                else
                {
                    record.Month = current.Month;
                    record.Year = current.Year;
                    context.SaveChanges();
                }
            }
        }
    }
}
