﻿
namespace External.Implementation.DataAccess
{
    public interface IUserRepository
    {
        User Get(string login);

        void Insert(User user);
        User GetByLogin(string login);
        bool UserExists(string login, string email);
    }
}
