﻿
namespace External.Implementation.DataAccess
{
    public interface ICurrentRepository
    {
        Current Get(int userId);

        void Update(Current current);
    }
}
