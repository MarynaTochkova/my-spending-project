﻿using System.Collections.Generic;

namespace External.Implementation.DataAccess
{
    public interface ISpendingTypeRepository
    {
        IEnumerable<SpendingType> Get(int userId);

        int Insert(SpendingType spType);
    }
}
