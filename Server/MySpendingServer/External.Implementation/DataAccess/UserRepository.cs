﻿using System;
using System.Linq;

namespace External.Implementation.DataAccess
{
    public class UserRepository : IUserRepository
    {
        public User Get(string login)
        {
            throw new NotImplementedException();
        }

        public void Insert(User user)
        {
            using (var context = new MySpendingEntities())
            {
                context.Users.Add(user);
                context.SaveChanges();
            }
        }

        public User GetByLogin(string login)
        {
            using (var context = new MySpendingEntities())
            {
                return context.Users.FirstOrDefault(x => x.Login == login);
            }
        }

        public bool UserExists(string login, string email)
        {
            using (var context = new MySpendingEntities())
            {
                return context.Users.Any(x => x.Email == email || x.Login == login);
            }
        }
    }
}
