﻿
namespace External.Mapper
{
    public interface IObjectMapper
    {
            TDest Map<TSrc, TDest>(TSrc source);

            TDest Map<TSrc, TDest>(TSrc source, TDest destination);
    }
}
