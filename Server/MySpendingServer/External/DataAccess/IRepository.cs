﻿namespace External.DataAccess
{
    public interface IRepository : IReadOnlyRepository
    {
        void Insert<TEntity>(TEntity entity) where TEntity : class;

        void Update<TEntity>(TEntity entity) where TEntity : class;

        void Delete<TEntity>(TEntity entity) where TEntity : class;
    }
}
