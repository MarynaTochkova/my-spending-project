﻿using System.Collections.Generic;

namespace External.DataAccess
{
    public interface IReadOnlyRepository
    {
        TEntity Single<TEntity, TId>(TId id) where TEntity: class ;

        TEntity SingleOrDefault<TEntity, TId>(TId id) where TEntity : class;

        IReadOnlyCollection<TEntity> Select<TEntity>(IQuery<TEntity> query) where TEntity : class;

    }
}
