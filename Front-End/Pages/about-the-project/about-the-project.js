
var collectionDivs = $(".common");
var buttons = $(".buttons");

function loopForDisplay () {
    for (var i = 0; i < collectionDivs.length; i++) {
        if ( $(collectionDivs[i]).css("display")== "none"){
            $(collectionDivs[i]).css("display", "block");
            if (i == 2) {
                $(buttons[0]).css("display", "inline-block");
                $(buttons[1]).css("display", "inline-block");
                clearInterval(timer);
            }
            break;
        }
    }
}
loopForDisplay ();

var timer = setInterval(loopForDisplay, 5000);

$(buttons[0]).click (function () {
    location.href = "../registration/registration.html";
});

$(buttons[1]).click (function () {
    location.href = "../../index.html";
});