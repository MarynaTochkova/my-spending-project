/**
 * Created by Maryna on 2/17/2016.
 */


var box = $('#forButtons');

box.click (function (event) {
    var target = $(event.target);

    if (target.attr('data-content') == "About The Project") {
        location.href = "Pages/about-the-project/about-the-project.html";
    }
    else if (target.attr('data-content') == "Log In To My Account") {
        location.href = "Pages/login/login.html";
    }
    else if (target.attr('data-content') == "Create New Account") {
        location.href = "Pages/registration/registration.html";
    }
});