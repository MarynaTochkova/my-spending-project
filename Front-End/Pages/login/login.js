/**
 * Created by Maryna on 4/10/2016.
 */

var inputs = $("input");
var inputLogin = inputs.eq(0);
var inputPas = inputs.eq(1);
var coverForError = $(".cover");
var errorDiv = coverForError.children().first();
errorDiv.css({"height": "10%", "width": "20%"});
var okError = coverForError.find("button");
var forgotButton = $("#forget");
var button = $("#button");

button.click(function () {
    if (inputLogin.val() && inputPas.val()) {
        var objectForAuth = {
            Login: inputLogin.val(),
            Password: inputPas.val()
        };
        makeRequestForLogin(urlObject.loginAndPassword, JSON.stringify(objectForAuth));
    }
});

forgotButton.onclick = function () {

};

var makeRequestForLogin = function (url, dataForSending) {

    startLoadingAnimation();

    inputs.each(function(  ) {
        $(this).prop('disabled', true);
    });

    $.ajax({
        type: 'post',
        url: url,
        data: dataForSending,
        contentType: "application/json; charset=utf-8",
        traditional: true,
        success: function(xml, textStatus, xhr) {
            if (xhr.status == 200 || xhr.status == 204) {
                var responseWithCookie = xml;
                if (responseWithCookie) {
                    var login = responseWithCookie.login;
                    localStorage.setItem('login', login);
                    stopLoadingAnimation();
                    location.href = "../account-main-page/account-main-page.html";
                }
            }
        },
        error: function (xml, textStatus, xhr) {
            stopLoadingAnimation();
            showError();
            inputs.each(function(  ) {
                $(this).prop('disabled', false);
            });
            console.log("Status" +" " + xhr);
        }
    });
};

var showError = function () {
    coverForError.css ("display", "inline-block");
    okError.click (function () {
        coverForError.css ("display", "none");
    });
};
