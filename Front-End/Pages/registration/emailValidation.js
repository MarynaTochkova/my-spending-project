/**
 * Created by Maryna on 4/12/2016.
 */
var status = true;

$(document).ready(function() {
    $('#email').blur(function() {
        if($(this).val() != '') {
            var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
            if(pattern.test($(this).val())){
                $(this).css({'border' : '1px solid #569b44'});
                status = true;
            } else {
                $(this).css({'border' : '1px solid #ff0000'});
                status = false;
            }
        } else {
            $(this).css({'border' : '1px solid #ff0000'});
            status = false;
        }
    });
});