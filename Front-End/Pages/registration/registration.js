var showingTooltip;

$('#registration').mouseover (function(e) {
    var target = $(e.target );

    var tooltip = target.attr('data-tooltip');
    if (!tooltip) return;

    var tooltipElem = $("<div>");
    tooltipElem.addClass('tooltip');
    tooltipElem.html(tooltip);
    tooltipElem.appendTo("body");

    var coords = target[0].getBoundingClientRect();

    var left = coords.left + (target[0].offsetWidth - tooltipElem[0].offsetWidth) / 2;
    if (left < 0) left = 0; // not to jump out the left side of the window

    var top = coords.top - tooltipElem[0].offsetHeight - 5;
    if (top < 0) { // not to jump out the left side of the window
        top = coords.top + target[0].offsetHeight + 5;
    }

    tooltipElem.css({"left": left + 'px', "top": top + 'px'});
    setTimeout(function () {tooltipElem.css("display", "none")}, 1800);
    showingTooltip = tooltipElem;
});

document.getElementById('registration').onmouseout = function(e) {

    if (showingTooltip) {
        $( showingTooltip ).remove();
        showingTooltip = null;
    }

};

var SignUpButton = $("#SignUpButton");
var inputLogin = $("input").eq(0);
var inputPassword = $("input").eq(1);

var inputEmail = $("input").eq(2);

var error = $(".cover").eq(0);
var okError = error.find("button");
var errorEmail = $(".cover").eq(1);
var okErrorEmail = errorEmail.find("button");
var errorLoginExist = $(".cover").eq(2);
var okErrorLoginExist = errorLoginExist.find("button");

SignUpButton.click(function () {
    if(isKyr(inputLogin.val()) !== true || isKyr(inputPassword.val()) !== true) {
        error.css("display", "inline-block");
        okError.click (function () {
            error.css("display", "none");
        });
    }
    else if(inputEmail.css ("borderColor") == "rgb(255, 0, 0)" || inputEmail.css("borderColor") == "#ff0000" || inputEmail.val() == "Your Email address" || !status) {
        errorEmail.css("display", "inline-block");
        okErrorEmail.click (function () {
            errorEmail.css("display", "none");
        });
    }
    else if (isKyr(inputLogin.val()) && status === "true" ||  isKyr(inputLogin.val()) && status === true && isKyr(inputPassword.val())) {
        var registrationObjectInfo = {
            login: inputLogin.val(),
            password: inputPassword.val(),
            email: inputEmail.val()
        };
        sendRegistrationDataToServer (urlObject.registrationInfo, JSON.stringify(registrationObjectInfo));
    }
});

/*checking for latin letters + numbers*/
var isKyr = function (str) {
    return /^\s*(\w+)\s*$/.test(str);
};


var sendRegistrationDataToServer = function (url, dataForSending) {
    startLoadingAnimation();
    $.ajax({
        type: 'post',
        url: url,
        data: dataForSending,
        contentType: "application/json; charset=utf-8",
        traditional: true,
        success: function(xml, textStatus, xhr) {
            if (xhr.status == 201 ) {
                stopLoadingAnimation();
                location.href = "../login/login.html";
            }

        },
        error: function (xml, textStatus, xhr) {
            if (xhr == "Conflict") {
                stopLoadingAnimation();
                console.log("Status" + " " + xhr);
                errorLoginExist.css("display", "inline-block");
                okErrorLoginExist.click (function () {
                    errorLoginExist.css("display", "none");
                });
            }
        }
    });
};



