
/*For caption of table we get current month and year*/

var table = document.getElementsByTagName('table')[0];
var caption = table.getElementsByTagName('caption')[0];
var inputForDate = document.querySelector("input[id=datepicker]");
var year;
var month;
var responseYear;
var responses;


function onclickForDate () {
    inputForDate.onclick = function () {
        $("#datepicker").datepicker("setDate", new Date(year, month, new Date().getDate()));
    };
}

var makeRequestForMonthYear = function (url) {

    $.ajax({
        type: 'get',
        url: url,
        contentType: "application/json; charset=utf-8",
        traditional: true,
        success: function(xml, textStatus, xhr) {
            if (xhr.status == 200 ) {
                responseYear = xml;
                if (responseYear) {
                    year = responseYear.year;
                    month = responseYear.month;
                    caption.textContent = transfromMonth(month) + ' ' + year;
                    onclickForDate();
                    var correctMonth = month + 1;
                    clone();
                    makeRequest(urlObject.spending + "?month" + "=" + correctMonth + "&" + "year" + "=" + year, pasteTR);
                }
                else {
                    location.href = "../Pages/create-new-month/create-new-month.html";
                }
            }
            else {
                console.log("Status" +" " + xhr.status);
            }
        },
        error: function(xml, textStatus, xhr) {
            console.log("Status" +" " + xhr.status);
        }
    });
};

var login = localStorage.getItem('login');
$("#welcomeString").children().eq(0).text("Welcome, " + login);

/*Communication with Server*/

window.onload = function () {
    makeRequestForMonthYear(urlObject.current);
    makeRequest(urlObject.kindsOfSpending, pasteOptions);
};

var tr = "";
var arrayId = [];
function optionsFromServerToDOMFormat (resp) {
    for (var i = 0; i<resp.length; i++) {
        var object = resp[i];
        for (var key in object) {
            if (key == "Id") {
                arrayId.push(object[key]);
            }
            else if(key == "Name") {
                tr+= "<option>" + "" + object[key] + "" + "</option>";
            }
        }
    }
}

/*dynamically know sizes*/
function getStyle (oElm, css3Prop){
    var strValue = "";
    if(window.getComputedStyle){
        strValue = getComputedStyle(oElm).getPropertyValue(css3Prop);
    }
    else if (oElm.currentStyle){
        try {
            strValue = oElm.currentStyle[css3Prop];
        } catch (e) {}
    }
    return strValue;
}


var sumDiv = document.getElementById("sum");
var coordCostCell = getCoords(table.tBodies[0].rows[0].cells[3]);
sumDiv.style.marginLeft = coordCostCell.left - getCoords(table).left/2 + "px";

var totalSumm = function () {
    var totalSum = 0;
    for (var i = 1; i < table.tBodies[0].rows.length -1; i++) {
        totalSum += +table.tBodies[0].rows[i].cells[3].innerHTML;
    }
    sumDiv.childNodes[2].data = totalSum.toFixed(2);
};

/*for hidden more than one string in group and show all strings when onmouse*/

var kindOfSpendingSelect;
var selectedGroup;
var condition = 0;
var selectOption;
var newKind;

function restartKindCalc() {
    kindOfSpendingSelect = document.getElementsByTagName("select")[0];
}
restartKindCalc();

var coverForErrorDiv = document.getElementsByClassName("tooltipError")[0].parentNode;
var errorOKbutton = coverForErrorDiv.getElementsByTagName("button")[0];
var addNewSpendingButton = document.querySelectorAll("button[class=addNewSpendingButton]")[0];
var addNewSpendingButtonCancel = document.querySelectorAll("button[class=addNewSpendingButton]")[1];
var coverForAddKindOfSpendingDiv = document.querySelector("div[id=addKindOfSpending]").parentNode;
var addNewSpendingInputId = document.querySelector("input[id=addNewSpendingInputId]");
var coverForErrorForNewKind = coverForErrorDiv.cloneNode(true);
var okForNewKind = coverForErrorForNewKind.getElementsByTagName("button")[0];
coverForErrorForNewKind.children[0].children[1].childNodes[0].data = "You can't save empty name";
coverForErrorForNewKind.children[0].children[1].childNodes[2].data = "Please, create name of spending that suits you";
var shortAlert = document.getElementById("shortAlert");
var coverForShortAlert = shortAlert.parentNode;

function onchangeKind(event) {
    if (!event) {
        event = window.event;
    }
    if (event.target == kindOfSpendingSelect) {
        for (var i = 0; i < kindOfSpendingSelect.options.length; i++) {
            var option = kindOfSpendingSelect.options[i];
            if (option.selected) {
                selectedGroup = option.text;
                selectOption = option;
                if (selectedGroup.length > 49) {
                    option.text = selectedGroup.substring(0, 47) + '...';
                    condition = 1;
                }
                break;
            }
        }
    }
}

function shortAlertMes () {
    coverForShortAlert.style.display = "none";
}

var Errors = function(kindError) {
    document.body.appendChild(kindError);
    kindError.style.display = "inline-block";
};

function okError (button, kindError) {
    button.onclick = function () {
        kindError.style.display = "none";
    };
}

function add (e) {
    if(e.target == addNewSpendingButton){
        if(addNewSpendingInputId.value) {
            newKind = document.createElement('option');
            newKind.text = addNewSpendingInputId.value;
            newKind.selected = true;
            var dataToSent = {Name: newKind.value};
            makeRequestForNewOption(urlObject.kindsOfSpending, JSON.stringify(dataToSent) );
            makeKindShort();
            coverForAddKindOfSpendingDiv.style.display = "none";
            coverForShortAlert.style.display = "inline-block";
            setTimeout(shortAlertMes, 1200);
            addNewSpendingInputId.value="";
        }
        else {
            coverForAddKindOfSpendingDiv.style.display = "none";
            Errors(coverForErrorForNewKind);
            okForNewKind.onclick = function () {
                coverForErrorForNewKind.style.display = "none";
                coverForAddKindOfSpendingDiv.style.display = "inline-block";
            };
        }
    }
    else if (e.target == addNewSpendingButtonCancel) {
        coverForAddKindOfSpendingDiv.style.display = "none";
    }
}

var makeRequestForNewOption = function (url, dataForSending) {

    $.ajax({
        type: 'post',
        url: url,
        data: dataForSending,
        contentType: "application/json; charset=utf-8",
        traditional: true,
        success: function(xml, textStatus, xhr) {
            if (xhr.status == 200) {
                var idOfNewKind = xml;
                kindOfSpendingSelect.options[kindOfSpendingSelect.options.length-1].id = "" + idOfNewKind + "";
                tr += "<option id=" +"" + String(idOfNewKind) + ""+">" + newKind.innerHTML + "</option>";
                kindOfSpendingSelect.innerHTML = tr;
                kindOfSpendingSelect.options[kindOfSpendingSelect.options.length-1].selected = true;
                tr = kindOfSpendingSelect.innerHTML;
            }
        },
        error: function (xml, textStatus, xhr) {
            console.log("Status" +" " + xhr.status);
        }
    });
};

function addNewSpendingKind () {
    document.addEventListener('click',add, true);
}

function onmouseoverKindOfSpending(event) {
    if (!event) {
        event = window.event;
    }
    if (event.target == kindOfSpendingSelect && condition) {
        selectOption.text = selectedGroup;
        condition = 0;
    }
}

function onmouseoutKindOfSpending(event) {
    if (!event) {
        event = window.event;
    }
    if (event.target == kindOfSpendingSelect) {
        if (!condition) {
            makeKindShort ();
        }
    }
}

function makeKindShort () {
    for (var i = 0; i < kindOfSpendingSelect.options.length; i++) {
        var option = kindOfSpendingSelect.options[i];
        if (option.selected) {
            selectedGroup = option.text;
            selectOption = option;
            if (selectedGroup.length > 49) {
                option.text = selectedGroup.substring(0, 47) + '...';
                condition = 1;
            }
            break;
        }
    }
}
/*for Tooltip in input fields*/

var showingTooltip;

function onmouseoverTooltip(e) {
    if (!e) {
        e = window.event;
    }
    if (showingTooltip){
        return;
    }
    var target = e.target;
    var tooltip = target.getAttribute('data-tooltip');
    if (!tooltip) return;
    var tooltipElem = document.createElement('div');
    tooltipElem.className = 'tooltip';
    tooltipElem.innerHTML = tooltip;
    document.body.appendChild(tooltipElem);
    var coords = target.getBoundingClientRect();
    var left = coords.left + (target.offsetWidth - tooltipElem.offsetWidth) / 2;
    if (left < 0) left = 0; // not to jump out the left side of the window
    var top = coords.top - tooltipElem.offsetHeight - 5;
    if (top < 0) { // not to jump out the left side of the window
        top = coords.top + target.offsetHeight + 5;
    }
    tooltipElem.style.left = left + 'px';
    tooltipElem.style.top = top + 'px';
    showingTooltip = tooltipElem;
    if(tooltip.length <=16) {
        setTimeout(onmouseoutTooltip, 1000);
    }
    else if (tooltip.length >16 && tooltip.length <=76){
        setTimeout(onmouseoutTooltip, 1800);
    }
    else if (tooltip.length > 76) {
        setTimeout(onmouseoutTooltip, 2500);
    }
}

function onmouseoutTooltip() {
    document.body.removeChild(showingTooltip);
    showingTooltip = null;
}

table.addEventListener("change", onchangeKind, true);
table.addEventListener("mouseover", onmouseoverKindOfSpending, true);
table.addEventListener("mouseout", onmouseoutKindOfSpending, true);
document.addEventListener("mouseover", onmouseoverTooltip, true);

/*for making buttons 100% height of cell*/

var actionButtonsForm = document.querySelector("div[id=actionButtonsForm]");
actionButtonsForm.style.height = actionButtonsForm.offsetHeight + 'px';
actionButtonsForm.style.minHeight = "30px";
actionButtonsForm.getElementsByTagName('button')[0].style.height = "100%";
actionButtonsForm.getElementsByTagName('button')[1].style.height = "100%";
actionButtonsForm.getElementsByTagName('button')[2].style.height = "100%";
actionButtonsForm.getElementsByTagName('button')[0].getElementsByTagName("img")[0].style.height = "100%";
actionButtonsForm.getElementsByTagName('button')[1].getElementsByTagName("img")[0].style.height = "100%";
actionButtonsForm.getElementsByTagName('button')[2].getElementsByTagName("img")[0].style.height = "100%";

/*validation of name*/

var inputName;
var nameErrors;
var wasClickName;

function restarName() {
    inputName = document.querySelector("input[id=inputNameOfSpendings]");
    nameErrors = true;
    wasClickName = 0;
}
restarName();

var validateName = function(e) {
    if (!e) {
        e = window.event;
    }
    if (e.target == inputName) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            errorOrMessageName();
        }
    }
};

function errorOrMessageName() {
    if (!inputName.value) {
        inputName.value = "Please fill out the field";
        inputName.classList.add("forValidationMassage");
        nameErrors = false;
    } else {
        nameErrors = true;
    }
}

function inputNameOnclick(e) {
    if (!e) {
        e = window.event;
    }
    if (e.target == inputName) {
        if (inputName.classList.contains("forValidationMassage")) {
            inputName.classList.remove("forValidationMassage");
            inputName.value = "";
        }
        wasClickName = true;
    }
}

function mouseoutInstedOfSubmit1(e) {
    if (!e) {
        e = window.event;
    }
    if (e.target == inputName) {
        if (wasClickName) {
            if (inputName.value != "Please fill out the field")
                errorOrMessageName();
        }
    }
}
table.addEventListener("keypress", validateName, true);
table.addEventListener("click", inputNameOnclick, true);
table.addEventListener("mouseout", mouseoutInstedOfSubmit1, true);

/*validation of cost*/

var inputCost;
var conditionDot;
var costErrors;
var wasClickCost;

function restartCost() {
    inputCost = document.querySelector("input[id=costOfSpendings]");
    conditionDot = true;
    costErrors = true;
    wasClickCost = 0;
}
restartCost();

var validateCost = function(event) {
    if (!event) {
        event = window.event;
    }
    if (event.target == inputCost) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            errorOrMessageCost();
        }
    }
};

function errorOrMessageCost() {

    if (!inputCost.value) {
        inputCost.value = "Please fill out the field";
        inputCost.classList.add("forValidationMassage");
        costErrors = false;
    } else if (inputCost.value) {
        for (var i = 0; i < inputCost.value.length; i++) {
            if (isNaN(parseFloat(inputCost.value[i]) && isFinite(inputCost.value[i])) && inputCost.value[i] == '.') {
                if (i === 0) {
                    inputCost.value = "Incorrect data";
                    inputCost.classList.add("forValidationMassage");
                    costErrors = false;
                    break;
                } else if (inputCost.value[i] == '.' && i !== 0) {
                    if (conditionDot) {
                        conditionDot = false;
                        continue;
                    } else if (!conditionDot) {
                        inputCost.value = "Incorrect data";
                        inputCost.classList.add("forValidationMassage");
                        costErrors = false;
                        conditionDot = true;
                        break;
                    }
                }
            } else if (isNaN(parseFloat(inputCost.value[i]) && isFinite(inputCost.value[i]))) {
                inputCost.value = "Incorrect data";
                inputCost.classList.add("forValidationMassage");
                costErrors = false;
                break;
            }
            costErrors = true;
        }
        conditionDot = true;
    }
}

function inputCostOnclick(event) {
    if (!event) {
        event = window.event;
    }
    if (event.target == inputCost) {
        if (inputCost.classList.contains("forValidationMassage")) {
            inputCost.classList.remove("forValidationMassage");
            inputCost.value = "";
        }
        wasClickCost = true;
    }
}

function mouseoutInstedOfSubmit(event) {
    if (!event) {
        event = window.event;
    }
    if (event.target == inputCost) {
        if (wasClickCost) {
            if (inputCost.value != "Please fill out the field")
                errorOrMessageCost();
        }
    }
}

table.addEventListener("keypress", validateCost, true);
table.addEventListener("click", inputCostOnclick, true);
table.addEventListener("mouseout", mouseoutInstedOfSubmit, true);

/*importance actions*/

var highButton;
var notHighButton;
var conditionOfImportance;

function restartButton() {
    highButton = document.querySelectorAll('button[name="high"]')[0];
    notHighButton = document.querySelectorAll('button[name="notHigh"]')[0];
    conditionOfImportance = 0;
}

restartButton();

function highButtonOnclick(e) {
    if (!e) {
        e = window.event;
    }
    var target = e.target;
    if (target == highButton) {
        var td = target.parentNode.parentNode;
        if (target.parentNode.parentNode.tagName == "TD") {
            td.innerHTML = "High importance";
            td.id = "highImportanceMassage";
            conditionOfImportance = 1;
        }
    }
}

table.addEventListener("click", highButtonOnclick, true);

function notHighButtonOnclick(e) {
    if (!e) {
        e = window.event;
    }
    var target = e.target;
    if (target == notHighButton) {
        var td = target.parentNode.parentNode;
        if (target.parentNode.parentNode.tagName == "TD") {
            td.innerHTML = "Not high importance";
            td.id = "notHighImportanceMassage";
            conditionOfImportance = 1;
        }
    }
}

table.addEventListener("click", notHighButtonOnclick, true);

/*get clone of row to insert it than*/

var cloneRow;
var row;
function clone() {
    row = table.rows[table.rows.length-1];
    cloneRow = row.cloneNode(true);
    if (cloneRow.children.length > 1) {
        cloneRow = row.cloneNode(true);
    } else if (cloneRow.children.length <= 1) {
        cloneRow = table.rows[table.rows.length - 1].cloneNode(true);
    }
}

/*for saving rows and automatically add new one + for error massage*/
var saveButton;
var selectDate;
var selectDateTd;
var kindOfSpendingSelectTd;
var inputNameTd;
var inputCostTd;
var importanceTd;
var editButton;
var deleteButton;
var saveDeleteEditeTd;
var saveButtonS;
var editButtonE;
var deleteButtonD;
var firstButtonForAddOption;

function firstButtonForAddOptionFunc (event) {
    if (!event) {
        event = window.event;
    }
    if (event.target == firstButtonForAddOption) {
        coverForAddKindOfSpendingDiv.style.display = "inline-block";
        addNewSpendingKind();
    }
}

table.addEventListener("click", firstButtonForAddOptionFunc, true);
function restartDataInVariables() {
    firstButtonForAddOption = document.querySelector("button[id=firstButtonForAddOption]");
    saveButtonS = document.querySelectorAll('button[name="save"]');
    saveButton = saveButtonS[saveButtonS.length - 1];
    editButtonE = document.querySelectorAll('button[name="edit"]');
    editButton = editButtonE[editButtonE.length - 1];
    deleteButtonD = document.querySelectorAll('button[name="delete"]');
    deleteButton = deleteButtonD[deleteButtonD.length - 1];
    importanceTd = document.querySelectorAll('td[id="importanceTd"]')[0];

    if (saveButton) {
        saveButton = saveButtonS[saveButtonS.length - 1];
    } else if (!saveButton) {
        saveButton = document.querySelectorAll('button[name="save"]')[0];
    }
    if (editButton) {
        editButton = editButtonE[editButtonE.length - 1];
    } else if (!editButton) {
        editButton = document.querySelectorAll('button[name="edit"]')[0];
    }
    if (deleteButton) {
        deleteButton = deleteButtonD[deleteButtonD.length - 1];
    } else if (!deleteButton) {
        deleteButton = document.querySelectorAll('button[name="delete"]')[0];
    }
    if (importanceTd) {
        importanceTd = document.querySelectorAll('td[id="importanceTd"]')[0];
    } else if (document.querySelectorAll('td[id="notHighImportanceMassage"]')[0]) {
        importanceTd = document.querySelectorAll('td[id="notHighImportanceMassage"]')[0];
    } else if (document.querySelectorAll('td[id="highImportanceMassage"]')[0]) {
        importanceTd = document.querySelectorAll('td[id="highImportanceMassage"]')[0];
    }

    selectDate = document.querySelector("input[id=datepicker]");
    selectDateTd = selectDate.parentNode.parentNode;
    kindOfSpendingSelectTd = kindOfSpendingSelect.parentNode;
    inputNameTd = inputName.parentNode;
    inputCostTd = inputCost.parentNode;
    saveDeleteEditeTd = document.querySelectorAll('div[id="actionButtonsForm"]')[0];
}

restartDataInVariables();

function onclickSave(event) {
    if (!event) {
        event = window.event;
    }
    if (event.target == saveButton || event.target.parentNode == saveButton) {
        errorOrMessageCost();
        errorOrMessageName();
        if (!costErrors || !nameErrors || selectDate.value == CONST.title) {
            stopErrorButton();
        } else if (!inputName.value || inputName.value == "Please fill out the field"|| !inputCost.value || !conditionOfImportance) {
            stopErrorButton();
        } else {
            correctingOnunload();
            clone();
            totalSumm();
            inputForDate = document.querySelector("input[id=datepicker]");
            inputForDate.removeAttribute("class");
            $(function() {
                $( "#datepicker" ).datepicker({
                    dateFormat: 'dd'
                });
            });
            onclickForDate();
        }
    }
}

table.addEventListener("click", onclickSave, true);

var idOfSpendingKind;
var correctingOnunload = function() {
    changeRowAfterSaving();
    saveButton.style.display = "none";
    table.tBodies[0].appendChild(cloneRow);
    restartKindCalc();
    kindOfSpendingSelect.innerHTML = tr;
    kindOfSpendingSelect.options[0].selected = true;
    restarName();
    restartCost();
    restartButton();
    restartDataInVariables();
};

var rowForId;

var changeRowAfterSaving = function () {
    changeRowCommon();
    sendObjectAfterSavingRow();

};

var changeRowCommon = function () {
    if (selectDate.value) {
        selectDateTd.innerHTML = selectDate.value;
    }
    else {
        selectDateTd.innerHTML = selectDate.children[0].value
    }
    selectDateTd.classList.add("forSavingText");
    selectedGroup = kindOfSpendingSelect.options[kindOfSpendingSelect.selectedIndex].text;
    idOfSpendingKind = kindOfSpendingSelect.options[kindOfSpendingSelect.selectedIndex].id;
    kindOfSpendingSelectTd.innerHTML = selectedGroup;
    kindOfSpendingSelectTd.classList.add("forSavingText");
    kindOfSpendingSelectTd.style.textAlign = "left";
    inputNameTd.innerHTML = inputName.value;
    inputNameTd.classList.add("forSavingText");
    inputCostTd.innerHTML = inputCost.value;
    inputCostTd.classList.add("forSavingText");
    importanceTd.removeAttribute("id");
    importanceTd.classList.add("forSavingText");
    editButton.style.display = "inline-block";
    deleteButton.style.display = "inline-block";
};

var sendObjectAfterSavingRow = function () {
    var d = new Date (year, month, selectDate.value);
    d.setHours(d.getHours() - d.getTimezoneOffset() / 60);
    var objectLikeARow = {
        Cost: inputCostTd.innerHTML,
        Date: d,
        IsImportant: (function () {
            if (importanceTd.innerHTML == "High importance") { return true; }
            else if (importanceTd.innerHTML == "Not high importance") { return false; }
        })(),
        Name: inputNameTd.innerHTML,
        SpendingTypeName: kindOfSpendingSelectTd.innerHTML,
        SpendingTypeId: idOfSpendingKind
    };
    rowForId = selectDateTd.parentNode;
    sendRowLikeObjectToServet (urlObject.spending, JSON.stringify(objectLikeARow));
};

var sendRowLikeObjectToServet = function (url, dataForSending) {

    $.ajax({
        type: 'post',
        url: url,
        data: dataForSending,
        contentType: "application/json; charset=utf-8",
        traditional: true,
        success: function(xml, textStatus, xhr) {
            if (xhr.status == 200 ) {
                var idOfNewRowLikeObject = xml;
                rowForId.id = idOfNewRowLikeObject;
            }
        },
        error: function (xml, textStatus, xhr) {
            console.log("Status" +" " + xhr.status);
        }
    });
};

var stopErrorButton = function() {
    coverForErrorDiv.style.display = "inline-block";
};

errorOKbutton.onclick = function() {
    coverForErrorDiv.style.display = "none";
    return false;
};

/*delete row*/

var CoverErrorForEditNotSavingCurrent = coverForErrorDiv.cloneNode(true);
var errorOKForEditNotSavingCurrent = CoverErrorForEditNotSavingCurrent.getElementsByTagName("button")[0];
CoverErrorForEditNotSavingCurrent.children[0].children[1].childNodes[0].data = "You can't delete spending if you didn't save current";
CoverErrorForEditNotSavingCurrent.children[0].children[1].childNodes[2].data = "Please, save current spending which you started to fill";
var buttonsForAcceptDelete = document.querySelectorAll('button[id="buttonsForAcceptDelete"]')[0];
var buttonsForCancelDelete = document.querySelectorAll('button[id="buttonsForCancelDelete"]')[0];
var deleteOrNotDiv = buttonsForCancelDelete.parentNode.parentNode;

function deleteButtonFunc (event) {
    if (!event) {
        event = window.event;
    }
    if(event.target.id == "deleteBut" || event.target.parentNode && event.target.parentNode.id == "deleteBut") {
        if (inputName.value || inputCost.value || !importanceTd.children.length || kindOfSpendingSelect.options[kindOfSpendingSelect.selectedIndex].id!=="1") {

            Errors(CoverErrorForEditNotSavingCurrent);
            okError(errorOKForEditNotSavingCurrent, CoverErrorForEditNotSavingCurrent);
        }
        else {
            for (var i = 0; i <= deleteButtonD.length; i++) {
                if (deleteButtonD[i] == event.target || deleteButtonD[i] == event.target.parentNode) {
                    deleteOrNotDiv.style.display = "inline-block";
                    var index = i;
                    buttonsForAcceptDelete.onclick = function () {
                        clone();
                         var urlDelete = urlObject.spending + "/delete/" + deleteButtonD[index].parentNode.parentNode.parentNode.id;
                        table.deleteRow(index + 1);
                        restartDataInVariables();
                        deleteOrNotDiv.style.display = "none";
                        makeRequestForDelete(urlDelete);
                        totalSumm();
                    };
                    buttonsForCancelDelete.onclick = function () {
                        deleteOrNotDiv.style.display = "none";
                    };
                    break;
                }
            }
        }
    }
}


var makeRequestForDelete = function (url) {

    $.ajax({
        type: 'delete',
        url: url,
        contentType: "application/json; charset=utf-8",
        traditional: true,
        success: function(xml, textStatus, xhr) {
            if (xhr.status == 200 || xhr.status == 204) {
                console.log("Deleted");
            }
        },
        error: function (xml, textStatus, xhr) {
            console.log("Status" +" " + xhr.status);
        }
    });
};

table.addEventListener("click", deleteButtonFunc, true);

/*edit row*/

var statusOfEdit = true;
var saveButtonAfterEdit = "";
var index = 0;

var errorForDoubleEdit = coverForErrorDiv.cloneNode(true);
var errorOKForDoubleEdit = errorForDoubleEdit.getElementsByTagName("button")[0];
errorForDoubleEdit.children[0].children[1].childNodes[0].data = "You can edit only one row in the same time";
errorForDoubleEdit.children[0].children[1].childNodes[2].data = "Please, save the row which you are already editing or filling";

function editButtonFunc (event) {
    if (!event) {
        event = window.event;
    }
    if(event.target.id == "editBut" || event.target.parentNode && event.target.parentNode.id == "editBut") {
        if (inputName.value || inputCost.value || !importanceTd.children.length || kindOfSpendingSelect.options[0].selected == false || inputForDate.value !== CONST.title ) {
            Errors(errorForDoubleEdit);
            okError(errorOKForDoubleEdit, errorForDoubleEdit);
        }
        else if (statusOfEdit) {
            restartDataInVariables();
            kindOfSpendingSelect.parentNode.children[1].style.display = "none";
            table.tBodies[0].children[table.tBodies[0].children.length-1].style.display = "none"; /*you can't create new when you are editing*/
            for (var i = 0; i <= editButtonE.length; i++) {
                if (editButtonE[i] == event.target || editButtonE[i] == event.target.parentNode) {
                    editButtonE[i].style.display = "none";
                    var rowForEditInformation = table.rows[i + 1].cloneNode(true);
                    var copyOfRowForStructure = table.tBodies[0].children[table.tBodies[0].children.length-1].cloneNode(true);

                    table.deleteRow(i + 1);
                    var tdAfter = table.rows[i + 1];
                    var parentTdAfter = table.rows[i + 1].parentNode;
                    parentTdAfter.insertBefore(copyOfRowForStructure, tdAfter);

                    table.rows[i+1].style.display = "";
                    var tdForAddingData = table.rows[i+1];

                    tdForAddingData.id = rowForEditInformation.id;

                    var dateSelect = tdForAddingData.cells[0].children[0];
                    dateSelect.children[0].value = rowForEditInformation.cells[0].innerHTML;
                    inputForDate = dateSelect.children[0];
                    inputForDate.removeAttribute("class");
                    $(function() {
                        $( "#datepicker" ).datepicker({
                            dateFormat: 'dd'
                        });
                    });

                    inputForDate.onclick = function () {
                        $("#datepicker").datepicker("setDate", new Date(year, month, new Date(year, month, inputForDate.value).getDate()));
                    };

                    var kindSelect = tdForAddingData.cells[1].children[0];
                    for (var n = 0; n < kindSelect.options.length; n++) {
                        if(rowForEditInformation.cells[1].innerHTML == kindSelect.options[n].innerHTML) {
                            kindSelect.options[n].selected = true;
                            break;
                        }
                    }

                    var nameInput = tdForAddingData.cells[2].children[0];
                    nameInput.value = rowForEditInformation.cells[2].innerHTML;

                    var costInput = tdForAddingData.cells[3].children[0];
                    costInput.value = rowForEditInformation.cells[3].innerHTML;

                    var importance = document.getElementById("importanceTd").cloneNode(true);
                    var actions = document.getElementById("actionButtonsForm").cloneNode(true);

                    $(editButtonE[i]).click(function(event) {
                        $(copyOfRowForStructure).addClass("highlight");
                        $(costInput).addClass("highlight");
                        $(nameInput).addClass("highlight");
                        $(inputForDate).addClass("highlight");
                        $(kindOfSpendingSelect).addClass("highlight");
                        $(copyOfRowForStructure.querySelector('button[id=saveBut]')).addClass("highlight");
                    });

                    break;
                }
            }
            kindOfSpendingSelect = kindSelect;
            selectedGroup = kindOfSpendingSelect.options[kindOfSpendingSelect.selectedIndex].text;
            inputName = nameInput;
            nameErrors = true;
            wasClickName = 0;
            inputCost = costInput;
            conditionDot = true;
            costErrors = true;
            wasClickCost = 0;
            highButton = tdForAddingData.cells[4].children[0].children[0];
            notHighButton = tdForAddingData.cells[4].children[0].children[1];
            conditionOfImportance = 0;
            saveButtonAfterEdit = tdForAddingData.cells[5].children[0].children[0];
            selectDate = tdForAddingData.cells[0].children[0];
            selectDateTd = tdForAddingData.cells[0];
            restartKind(tdForAddingData);
            inputNameTd = tdForAddingData.cells[2];
            inputCostTd = tdForAddingData.cells[3];
            importanceTd = tdForAddingData.cells[4];
            deleteButton = tdForAddingData.cells[5].children[0].children[2];
            editButton = tdForAddingData.cells[5].children[0].children[1];
            saveDeleteEditeTd = tdForAddingData.cells[5];
            statusOfEdit = false;
        }
    }
}

var restartKind = function (tr) {
    kindOfSpendingSelectTd = tr.cells[1];
    return kindOfSpendingSelectTd;
};

table.addEventListener("click", editButtonFunc, true);

function saveButtonFuncAfterEdit (event) {
    if (!event) {
        event = window.event;
    }
    if(event.target == saveButtonAfterEdit || event.target.parentNode == saveButtonAfterEdit) {
        errorOrMessageCost();
        errorOrMessageName();
        if (!costErrors || !nameErrors || selectDate.value == CONST.title) {
            stopErrorButton();
        }
        else if (!inputName.value || !inputCost.value || inputName.value == "Please fill out the field" || !conditionOfImportance) {
            stopErrorButton();
        }
        else {
            changeRowCommon();

            $(event.target.parentNode.parentNode.parentNode.parentNode).removeClass("highlight");
            $(event.target.parentNode.parentNode.parentNode).removeClass("highlight");

            saveButtonAfterEdit.style.display = "none";
            sendObjectAfterEditRow(saveButtonAfterEdit.parentNode.parentNode.parentNode);
            restartKindCalc();
            kindOfSpendingSelect.innerHTML = tr;
            kindOfSpendingSelect.options[0].selected = true;
            restarName();
            restartCost();
            restartButton();
            restartDataInVariables();
            inputForDate = document.querySelector("input[id=datepicker]");
            totalSumm();
            statusOfEdit = true;
            saveButtonAfterEdit = 0;
            index = 0;
            table.tBodies[0].children[table.tBodies[0].children.length - 1].style.display = "";
            kindOfSpendingSelect.parentNode.children[1].style.display = "";
            clone();
        }
    }
}


var sendObjectAfterEditRow = function (tr) {
    var d = new Date (year, month, tr.cells[0].innerHTML);
    d.setHours(d.getHours() - d.getTimezoneOffset() / 60);
    var objectLikeARowEdited = {
        Cost: inputCostTd.innerHTML,
        Date: d,
        IsImportant: (function () {
            if (importanceTd.innerHTML == "High importance") { return true; }
            else if (importanceTd.innerHTML == "Not high importance") { return false; }
        })(),
        Name: inputNameTd.innerHTML,
        SpendingTypeName: restartKind(tr).innerHTML,
        SpendingTypeId: idOfSpendingKind,
        Id: selectDateTd.parentNode.id
    };
    console.log(objectLikeARowEdited.Id);
    sendEditedRowLikeObjectToServet (urlObject.spending, JSON.stringify(objectLikeARowEdited));
};

var sendEditedRowLikeObjectToServet = function (url, dataForSending) {

    $.ajax({
        type: 'put',
        url: url,
        data: dataForSending,
        contentType: "application/json; charset=utf-8",
        traditional: true,
        success: function(xml, textStatus, xhr) {
            if (xhr.status == 200 || xhr.status == 204) {
                console.log("Edited");
            }
        },
        error: function (xml, textStatus, xhr) {
            console.log("Status" +" " + xhr.status);
        }
    });
};

table.addEventListener("click", saveButtonFuncAfterEdit, true);

/*saving all table*/

var navigationButtonsToAccount = document.getElementById("navigationButtonsToAccount");

navigationButtonsToAccount.onclick = function  () {
    location.href = "../account-main-page/account-main-page.html";
};


/*logOut*/
var logOutButton = $("#LogOut");
logOutButton.click (function () {
    sendLogOutToServer(urlObject.logout);
});

