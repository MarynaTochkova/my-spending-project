/**
 * Created by Maryna on 4/6/2016.
 */

var login = localStorage.getItem('login');
$("#welcomeString").children().eq(0).text("Welcome, " + login);

var table = document.getElementsByTagName("table")[0];
var welcomeDiv = document.querySelector("div[id=welcomeToArchive]");
var editButton = document.querySelector("div[id=editButton]");

var mainPart;
var menu;
var footer;

function variables () {
    mainPart = document.getElementById("mainPartForTable");
    menu = document.getElementById("listOfMonths");
    footer = document.querySelector("div[id=footer]");
}

function changeHeightMenu () {
    menu.style.height = mainPart.offsetHeight + "px";
}

function startHeightMenu () {
    menu.style.height = getCoords(footer).top - getCoords(mainPart).top - 10 + "px";
    menu.style.minHeight = menu.style.height;
}

var deleteRows = function () {
    for (var i = 1; i < table.rows.length; i++) {
        table.deleteRow(1);
    }
};

/*get and put months from server*/
window.onload = function () {
    makeRequestForMonthYearList(urlObject.month, pasteList);
};

var monthListDiv;

function listOfMonthsYears (xml) {
    var response = xml;
    if (response.length) {
        $("#listOfMonths").css("display", "block");
        monthListDiv = document.getElementById("listOfMonths");
        onclickList (); 
        for (var i = 0; i < response.length; i++) {
            var year = response[i].year;
            var month = response[i].month-1;
            var div = document.createElement('div');
            div.classList.add("listLinks");
            div.innerHTML = "" + transfromMonth(month) + " " + year;
            monthListDiv.appendChild(div);
            variables();
        }
        variables();
        startHeightMenu();
    }
    else {
            $('<span/>', {
            text: "For now you don't have history",
            class: "notHavingHistory"
        }).appendTo('#welcomeToArchive');
    }
}

/*for total sum*/

function coordinatesForTotalSum () {
    var sumDiv = document.querySelector("div[id=sum]");
    var coordCostCell = getCoords(table.tBodies[0].rows[table.tBodies[0].rows.length - 1].cells[3]);
    sumDiv.style.marginLeft = coordCostCell.left - getCoords(monthListDiv).left - monthListDiv.offsetWidth + "px";
    return sumDiv;
}

function totalSumm (divForSum) {
    var totalSum = 0;
    for (var i = 1; i <= table.tBodies[0].rows.length -1; i++) {
        totalSum += +table.tBodies[0].rows[i].cells[3].innerHTML;
    }
    divForSum.children().eq(0).text("Total:" + CONST.symbol_space + totalSum.toFixed(2));
}

/*button To Account*/
var navigationButtonsToAccount = document.getElementById("navigationButtonsToAccount");

navigationButtonsToAccount.onclick = function  () {
    location.href = "../account-main-page/account-main-page.html";
};

var logOut = document.querySelector("button[id=LogOut]");

var deleteTh = function () {
    for (var i = 0; i < table.rows.length; i++) {
        if (table.rows[i].id !== "headerTable") {
            table.deleteRow(i);
            i = 0;
        }
    }
};

function changeStyle (target) {
    for (var i = 0; i < monthListDiv.children.length; i++) {
        if(monthListDiv.children[i] == target) {
            target.classList.add("listSelectedStyle");
        }
        else  {
            monthListDiv.children[i].classList.remove("listSelectedStyle");
        }
    }
}

var year;
var month;

function onclickList () {
    monthListDiv.onclick = function (event) {
        if (!event) {
            event = window.event;
        }

        if (event.target.classList.contains("listLinks")) {
            welcomeDiv.style.display = "none";
            changeStyle(event.target);
            table.style.display = "table";
            var sumDiv = coordinatesForTotalSum();
            sumDiv.style.display = "block";
            editButton.style.display = "inline-block";
            deleteTh();
            var monthLet = event.target.innerHTML.split(" ")[0];
            year = event.target.innerHTML.split(" ")[1];
            month = transfromMonth(monthLet);
            getDataForArchive(month + 1, year);
            changeHeightMenu();
            var tableCapt = table.getElementsByTagName("caption")[0];
            tableCapt.innerText = monthLet + " " + year;
        }
    };
}

var getDataForArchive = function (m, y) {
    makeRequest(urlObject.spending + "?month"+"=" + m +"&" + "year" + "=" + y, pasteTR);
};

editButton.onclick = function () {
    var objectCurrentDate = {year:year, month:month};
    sendCurrentYearToServer(urlObject.current, JSON.stringify(objectCurrentDate));
};


/*logOut*/
var logOutButton = document.querySelector("button[id=LogOut]");
logOutButton.onclick = function () {
    sendLogOutToServer(urlObject.logout);
};