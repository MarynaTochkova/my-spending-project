/**
 * Created by Maryna on 5/6/2016.
 */


table.onclick = function(event) {
    if (!event) {
        event = window.event;
    }
        if (event.target.tagName != 'TH') return;

    if (event.target.hasAttribute('data-type')) {
        sortGrid(event.target, event.target.cellIndex, event.target.getAttribute('data-type'));
    }
};

var statusSort = {
    dateTh: true,
    kindTh: true,
    costTh: true,
    importTh: true
};

function sortGrid(target, colNum, type) {
    var tbody = table.getElementsByTagName('tbody')[0];
    var lastRow = tbody.rows[tbody.rows.length-1];
    var firstRow = tbody.rows[0];
    if (document.getElementsByClassName("cover").length) {
        tbody.deleteRow(tbody.rows.length-1);
    }

    tbody.deleteRow(0);
    var rowsArray = [].slice.call(tbody.rows);

    var compare;

    for (var key in statusSort) {
        if (target.id == key) {
            if (statusSort[key] == true) {
                sortDecr (type);
            }
            else if (statusSort[key] == false) {
                sortIncr (type);
            }
            break;
        }
    }

    function sortDecr (type) {
        switch (type) {
            case 'string':
                compare = function(rowA, rowB) {
                    if(rowA.cells[colNum].innerHTML < rowB.cells[colNum].innerHTML) {
                        return 1;
                    }
                    else if (rowA.cells[colNum].innerHTML >= rowB.cells[colNum].innerHTML) {
                        return -1;
                    }
                    else {
                        return 0;
                    }

                };

                break;

            case 'number':
                compare = function(rowA, rowB) {
                    return rowA.cells[colNum].innerHTML - rowB.cells[colNum].innerHTML;
                };

                break;
        }
        for (var key in statusSort) {
            if (target.id == key) {
                statusSort[key] = false;
                target.childNodes[0].nodeValue = target.childNodes[0].nodeValue.substring(0,target.childNodes[0].length-1);
                var arrowUp = $('<textarea />').html("&#9650;").text();
                target.childNodes[0].nodeValue = target.childNodes[0].nodeValue + arrowUp;
                break;
            }
        }
    }

    function sortIncr(type) {
        switch (type) {
            case 'string':
                compare = function(rowA, rowB) {
                    if(rowA.cells[colNum].innerHTML > rowB.cells[colNum].innerHTML) {
                        return 1;
                    }
                    else if (rowA.cells[colNum].innerHTML <= rowB.cells[colNum].innerHTML) {
                        return -1;
                    }
                    else {
                        return 0;
                    }

                };

                break;
            case 'number':
                compare = function(rowA, rowB) {
                    return rowB.cells[colNum].innerHTML - rowA.cells[colNum].innerHTML;
                };

                break;
        }
        for (var key in statusSort) {
            if (target.id == key) {
                statusSort[key] = true;
                target.childNodes[0].nodeValue = target.childNodes[0].nodeValue.substring(0,target.childNodes[0].length-1);
                var arrowUp = $('<textarea />').html("&#9660;").text();
                target.childNodes[0].nodeValue = target.childNodes[0].nodeValue + arrowUp;
                break;
            }
        }
    }

    rowsArray.sort(compare);
    table.removeChild(tbody);

    for (var i = 0; i < rowsArray.length; i++) {
        tbody.appendChild(rowsArray[i]);
    }

    var theFirstChild = tbody.firstChild;
    tbody.insertBefore(firstRow, theFirstChild);

    if (document.getElementsByClassName("cover").length) {
        tbody.appendChild(lastRow);
    }

    table.appendChild(tbody);
}