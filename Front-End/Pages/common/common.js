/**
 * Created by Maryna on 4/13/2016.
 */
var urlObject = {
    kindsOfSpending: "http://localhost:9000/api/types",
    spending: "http://localhost:9000/api/spending",
    loginAndPassword: "http://localhost:9000/api/auth/login",
    current: "http://localhost:9000/api/current",
    month: "http://localhost:9000/api/month",
    registrationInfo: "http://localhost:9000/api/user",
    logout: "http://localhost:9000/api/auth/logout"
};

var CONST = {
    title: $('<textarea />').html("&#5121;").text(),
    symbol_space: $('<textarea />').html("&nbsp;").text()
};

var sendCurrentYearToServer = function (url, dataForSending) {

    $.ajax({
        type: 'post',
        url: url,
        data: dataForSending,
        contentType: "application/json; charset=utf-8",
        traditional: true,
        success: function(xml, textStatus, xhr) {
            if (xhr.status == 200 || xhr.status == 204) {
                location.href = "../last-edited-month/last-edited-month.html";
            }
        },
        error: function (xml, textStatus, xhr) {
            console.log("Status" +" " + xhr.status);
        }
    });
};


function transfromMonth (monthOrIndex) {
    if(typeof monthOrIndex == "number") {
        index = monthOrIndex;
        if (index == "0") {
            return "January";
        }
        else if (index == "1") {
            return "February";
        }
        else if (index == "2") {
            return "March";
        }
        else if (index == "3") {
            return "April";
        }
        else if (index == "4") {
            return "May";
        }
        else if (index == "5") {
            return "June";
        }
        else if (index == "6") {
            return "July";
        }
        else if (index == "7") {
            return "August";
        }
        else if (index == "8") {
            return "September";
        }
        else if (index == "9") {
            return "October";
        }
        else if (index == "10") {
            return "November";
        }
        else if (index == "11") {
            return "December";
        }
    }
    else if(typeof monthOrIndex == "string") {
        month = monthOrIndex;
        if (month == "January") {
            return 0;
        }
        else if (month == "February") {
            return 1;
        }
        else if (month == "March") {
            return 2;
        }
        else if (month == "April") {
            return 3;
        }
        else if (month == "May") {
            return 4;
        }
        else if (month == "June") {
            return 5;
        }
        else if (month == "July") {
            return 6;
        }
        else if (month == "August") {
            return 7;
        }
        else if (month == "September") {
            return 8;
        }
        else if (month == "October") {
            return 9;
        }
        else if (month == "November") {
            return 10;
        }
        else if (month == "December") {
            return 11;
        }
    }
}

/*get and past spending from Server*/

var makeRequest = function (url, callback) {

    var callback = callback;
    $.ajax({
        type: 'get',
        url: url,
        contentType: "application/json; charset=utf-8",
        traditional: true,
        success: function(xml, textStatus, xhr) {
            if (xhr.status == 200) {
                if (callback == "pasteTR") {
                    responseTr = xml;
                    if (responseTr) {
                        for (var i = 0; i < responseTr.length; i++) {
                            var object = responseTr[i];
                            var trBasic = table.rows[table.rows.length - 1].cloneNode(true);
                            table.tBodies[0].insertBefore(trBasic, table.tBodies[0].rows[table.tBodies[0].rows.length - 1]);
                            for (var key in object) {
                                if (key == "Date") {
                                    trBasic.cells[0].innerHTML = new Date(object[key]).getDate();
                                    trBasic.cells[0].classList.add("forSavingText");
                                }
                                if (key == "SpendingTypeName") {
                                    trBasic.cells[1].innerHTML = object[key];
                                    trBasic.cells[1].classList.add("forSavingText");
                                    trBasic.cells[1].style.textAlign = "left";
                                }
                                if (key == "SpendingTypeId") {
                                    trBasic.cells[1].id = object[key].toString();
                                }
                                if (key == "Name") {
                                    trBasic.cells[2].innerHTML = object[key];
                                    trBasic.cells[2].classList.add("forSavingText");
                                }
                                if (key == "Cost") {
                                    trBasic.cells[3].innerHTML = object[key];
                                    trBasic.cells[3].classList.add("forSavingText");
                                }
                                if (key == "IsImportant") {
                                    if (object[key]) {
                                        trBasic.cells[4].innerHTML = "High importance";
                                    } else if (!object[key]) {
                                        trBasic.cells[4].innerHTML = "Not high importance";
                                    }
                                    trBasic.cells[4].classList.add("forSavingText");
                                    trBasic.cells[4].removeAttribute("id");
                                }
                                if (key == "Id") {
                                    trBasic.id = "" + object[key] + "";
                                }
                            }
                            if (trBasic.cells[5]) {
                                trBasic.cells[5].getElementsByTagName("button")[0].style.display = "none";
                                trBasic.cells[5].getElementsByTagName("button")[1].style.display = "";
                                trBasic.cells[5].getElementsByTagName("button")[2].style.display = "";
                            }
                        }
                        if (trBasic) {
                            if (trBasic.cells[5]) {
                                totalSumm();
                            }
                            if (!trBasic.cells[5]) {
                                var rowTh = table.tBodies[0].rows[table.tBodies[0].rows.length - 1];
                                table.tBodies[0].insertBefore(rowTh, table.tBodies[0].children[0]);
                                totalSumm($("#sum"));
                            }
                        }
                        if (table.tBodies[0].rows[1].cells[5]) {
                            restartDataInVariables();
                        }
                    }
                }
                else if (callback == "pasteOptions") {
                    responses = xml;
                    if (responses) {
                        optionsFromServerToDOMFormat(responses);
                        kindOfSpendingSelect.innerHTML = tr;
                        kindOfSpendingSelect.options[0].selected = true;
                        addNewSpendingInputId.style.fontSize = getStyle(kindOfSpendingSelect.options[1], "font-size");
                        for (var i = 0; i < kindOfSpendingSelect.options.length; i++) {
                            kindOfSpendingSelect.options[i].setAttribute("id", arrayId[i]);
                        }
                        tr = kindOfSpendingSelect.innerHTML;
                    }
                }

            }
        },
        error: function (xml, textStatus, xhr) {
            console.log("Status" +" " + xhr.status);
        }
    });
};

var pasteOptions = "pasteOptions";
var pasteTR = "pasteTR";
var responseTr;

function getCoords(elem) {
    var box = elem.getBoundingClientRect();
    var body = document.body;
    var docEl = document.documentElement;
    var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
    var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;
    var clientTop = docEl.clientTop || body.clientTop || 0;
    var clientLeft = docEl.clientLeft || body.clientLeft || 0;
    var top = box.top + scrollTop - clientTop;
    var left = box.left + scrollLeft - clientLeft;
    return {
        top: top,
        left: left
    };
}

var sendLogOutToServer = function (url) {

    $.ajax({
        type: 'get',
        url: url,
        contentType: "application/json; charset=utf-8",
        traditional: true,
        success: function(xml, textStatus, xhr) {
            if (xhr.status == 200 || xhr.status == 204) {
                localStorage.removeItem("login");
                location.href = "../../index.html";
            }
            else {
                console.log("Status" +" " + xhr.status);
            }
        },
        error: function(xml, textStatus, xhr) {
            console.log("Status" +" " + xhr.status);
        }
    });
};

var masivOfYearMonth = "masivOfYearMonth";
var pasteList = "pasteList";

var makeRequestForMonthYearList = function (url, callback) {

    var callback = callback;

    $.ajax({
        type: 'get',
        url: url,
        contentType: "application/json; charset=utf-8",
        traditional: true,
        success: function(xml, textStatus, xhr) {
            if (xhr.status == 200) {
                if (callback == "masivOfYearMonth") {
                    errorOrCreate (xml);
                }
                if (callback == "pasteList") {
                    listOfMonthsYears (xml);
                }
            }
        },
        error: function(xml, textStatus, xhr) {
            console.log("Status" +" " + xhr.status);
            variables();
            startHeightMenu();
        }
    });
};

/*for animation onload*/
function startLoadingAnimation() {
    var imgObj = $("#loadImg");
    imgObj.show();

    var centerY = $(window).scrollTop() + ($(window).height() + imgObj.height())/2;
    var centerX = $(window).scrollLeft() + ($(window).width() + imgObj.width())/2;

    imgObj.offset({"top": centerY, "left": centerX});
}

function stopLoadingAnimation() {
    $("#loadImg").hide();
}