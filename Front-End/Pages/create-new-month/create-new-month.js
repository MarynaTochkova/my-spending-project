/**
 * Created by Maryna on 2/23/2016.
 */

var selectYear = $('[name="menuYear"]')[0];
var selectMonth = $('[name="menuMonth"]')[0];

var login = localStorage.getItem('login');
$("#welcomeString").children().eq(0).text("Welcome, " + login);

var year;

function changeYear () {

    for (var i = 0; i < selectYear.options.length; i++) {
        var option_y = selectYear.options[i];
        if (option_y.selected) {
            year = option_y.text;
            break;
        }
    }
}

function changeMonth () {
    for (var i = 0; i < selectMonth.options.length; i++) {
        var option_m = selectMonth.options[i];
        if (option_m.selected) {
           return transfromMonth(option_m.text);
        }
    }
}

var buttonCreate =$('#buttonForCreateNewMonth');

var navigationButtonsToAccount = $("#navigationButtonsToAccount");

navigationButtonsToAccount.click (function  () {
    location.href = "../account-main-page/account-main-page.html";
});

/*logOut*/
var logOutButton = $("#LogOut");
logOutButton.click(function () {
    sendLogOutToServer(urlObject.logout);
});

/*do we have this month already?*/

var error = $(".cover");
var okError = error.find("button:first")[0];

function errorOrCreate (xml) {
    var responses = xml;
    changeYear();
    var month = changeMonth();
    if (responses) {
        for (var i = 0; i < responses.length; i++) {
            var yearFromHistory = responses[i].year;
            var monthFromHistory = responses[i].month - 1;
            if (yearFromHistory == year && monthFromHistory == month) {
                error.css("display","inline-block");
                okError.onclick = function () {
                    error.css("display","none");
                };
                return;
            }
        }
        var objectCurrentDate = {year: year, month: month};
        sendCurrentYearToServer(urlObject.current, JSON.stringify(objectCurrentDate));
    }
}

buttonCreate.click (function () {
    makeRequestForMonthYearList(urlObject.month, masivOfYearMonth);
});
