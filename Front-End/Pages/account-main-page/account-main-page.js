

var menu = $('#menu');
var login = localStorage.getItem('login');
$("#welcomeString").children().eq(0).text("Welcome, " + login);

menu.click(function (event) {
    var target =  event.target;
    if(target.getAttribute('data-content')=='currentMonth') {
        makeRequestDoWeHaveLast(urlObject.current);
    }
    else if(target.getAttribute('data-content')=='newMonth'){
        location.href = "../create-new-month/create-new-month.html";
    }
    else if(target.getAttribute('data-content')=='archive'){
        location.href = "../history/history.html";
    }
});


/*logOut*/
var logOutButton = $("#LogOut");
logOutButton.click(function () {
    sendLogOutToServer(urlObject.logout);
});

var makeRequestDoWeHaveLast = function (url) {

    $.ajax({
        type: 'get',
        url: url,
        contentType: "application/json; charset=utf-8",
        traditional: false,
        success: function(xml, textStatus, xhr) {
            if (xhr.status == 200 ) {
                responseYear = xml;
                if (!responseYear) {
                    location.href = "../create-new-month/create-new-month.html";
                }
                else {
                    location.href = "../last-edited-month/last-edited-month.html";
                }
            }
            else {
                console.log("Status" +" " + xhr.status);
            }
        },
        error: function(xml, textStatus, xhr) {
            console.log("Status" +" " + xhr.status);
        }
    });
};